# authlink

Testing auth link

## How to rebuild from scratch

### Firebase project

* Create a new Firebase project on https://console.firebase.google.com/
* Create a new iOS app in your new Firebase project (this repo is using the bundle ID com.authlink.test). Set your Team ID and a Store ID.
* [TEST LINK] In Firebase Authentication, enable the provider Email/Password and the Email link option
* [TEST LINK] In Firebase Dynamic Links, enable the Google managed domain (yourapp.page.link)
* [TEST APPLE] Follow https://firebase.google.com/docs/auth/ios/apple#configure_sign_in_with_apple

### iOS configuration

* Checkout this repository on a Mac
* Replace ios/GoogleService-Info.plist with yours
* In XCode, replace the signing team and bundle identifier with yours
* [TEST LINK] In Capabilities, change the associated domain to your dynamic link domain
* [TEST LINK] In the Info tab, change the authlink URL type to have your bundle ID as URL scheme
* [TEST LINK] In the Info tab, change FirebaseDynamicLinksCustomDomains item to your dynamic link domain
* [TEST APPLE] Enable the Sign-in with Apple capability
