import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

abstract class AuthDisconnect {
  Widget buildDisconnect() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text("You're currently authenticated."),
        Text('Email: ${FirebaseAuth.instance.currentUser?.email}'),
        Text('Name: ${FirebaseAuth.instance.currentUser?.displayName}'),
        const SizedBox(height: 8),
        ElevatedButton(
          child: const Text('DISCONNECT'),
          onPressed: FirebaseAuth.instance.signOut,
        ),
      ],
    );
  }
}
