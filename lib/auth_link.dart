import 'package:authlink/auth_disconnect.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';

class AuthLinkPage extends StatefulWidget {
  const AuthLinkPage({Key? key}) : super(key: key);

  @override
  State<AuthLinkPage> createState() => _AuthLinkPageState();
}

class _AuthLinkPageState extends State<AuthLinkPage> with AuthDisconnect {
  final _formKey = GlobalKey<FormState>();
  String? _email;
  bool _authenticated = false;
  bool _customDomain = true;

  @override
  void initState() {
    super.initState();
    _initDynamicLinks();
    FirebaseAuth.instance.authStateChanges().listen((User? user) async {
      setState(() {
        _authenticated = user != null;
      });
    });
  }

  String _getDynamicLinkDomain() {
    return _customDomain
        ? 'link.debug.readdress.app'
        : 'authtest2.page.link';
  }

  Future<void> _initDynamicLinks() async {
    FirebaseDynamicLinks.instance.onLink(
      onSuccess: (PendingDynamicLinkData? dynamicLink) async {
        print('AUTHLINK DEBUG: FirebaseDynamicLinks.instance.onLink.onSuccess');
        if (dynamicLink == null) {
          print('AUTHLINK DEBUG: Link is empty');
        } else {
          _handleDynamicLink(dynamicLink.link);
        }
      },
      onError: (OnLinkErrorException exception) async {
        throw exception;
      },
    );

    final PendingDynamicLinkData? data = await FirebaseDynamicLinks.instance.getInitialLink();
    if (data != null) {
      _handleDynamicLink(data.link);
    }
  }

  Future<void> _handleDynamicLink(Uri link) async {
    if (FirebaseAuth.instance.isSignInWithEmailLink(link.toString())) {
      await FirebaseAuth.instance.signInWithEmailLink(
        email: _email!,
        emailLink: link.toString(),
      );
    } else {
      print('AUTHLINK DEBUG: Dynamic link not recognized');
    }
  }

  Future<void> _handleSendEmail() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      await FirebaseAuth.instance.sendSignInLinkToEmail(
        email: _email!,
        actionCodeSettings: ActionCodeSettings(
          dynamicLinkDomain: _getDynamicLinkDomain(),
          url: 'https://readdress.app/',
          iOSBundleId: 'com.authlink.test',
          androidPackageName: 'com.authlink.test',
          androidInstallApp: true,
          handleCodeInApp: true,
        ),
      );

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Auth link sent with domain ${_getDynamicLinkDomain()}'),
      ));
    }
  }

  _buildLoginForm(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text("You're currently disconnected."),
          TextFormField(
            decoration: const InputDecoration(
              labelText: 'Enter your email',
            ),
            keyboardType: TextInputType.emailAddress,
            validator: (value) {
              if (value!.isEmpty) {
                return 'This field is required';
              }
              final regex = RegExp(r'[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,64}');
              if (!regex.hasMatch(value)) {
                return 'Invalid email format';
              }
            },
            onSaved: (String? email) {
              _email = email?.toLowerCase();
            },
          ),
          const SizedBox(height: 8),
          SwitchListTile(
            contentPadding: EdgeInsets.zero,
            title: const Text('Custom dynamic link domain'),
            value: _customDomain,
            onChanged: (bool checked) {
              setState(() {
                _customDomain = checked;
              });
            },
          ),
          ElevatedButton(
            child: const Text('SEND EMAIL'),
            onPressed: _handleSendEmail,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Testing Auth Link'),
      ),
      body: Center(
        child: FractionallySizedBox(
          widthFactor: 0.8,
          child: !_authenticated
              ? _buildLoginForm(context)
              : buildDisconnect(),
        ),
      ),
    );
  }
}
