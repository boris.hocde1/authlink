import 'dart:convert';
import 'dart:math';

import 'package:authlink/auth_disconnect.dart';
import 'package:crypto/crypto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class AuthApplePage extends StatefulWidget {
  const AuthApplePage({Key? key}) : super(key: key);

  @override
  State<AuthApplePage> createState() => _AuthApplePageState();
}

class _AuthApplePageState extends State<AuthApplePage> with AuthDisconnect {
  bool _authenticated = false;

  @override
  void initState() {
    super.initState();
    FirebaseAuth.instance.authStateChanges().listen((User? user) async {
      setState(() {
        _authenticated = user != null;
      });
    });
  }

  /// Generates a cryptographically secure random nonce, to be included in a
  /// credential request.
  String _generateNonce([int length = 32]) {
    const charset =
        '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._';
    final random = Random.secure();
    return List.generate(length, (_) => charset[random.nextInt(charset.length)])
        .join();
  }

  /// Returns the sha256 hash of [input] in hex notation.
  String _sha256ofString(String input) {
    final bytes = utf8.encode(input);
    final digest = sha256.convert(bytes);
    return digest.toString();
  }

  Future<void> _handleAppleSignIn() async {
    // To prevent replay attacks with the credential returned from Apple, we
    // include a nonce in the credential request. When signing in with
    // Firebase, the nonce in the id token returned by Apple, is expected to
    // match the sha256 hash of `rawNonce`.
    final rawNonce = _generateNonce();
    final nonce = _sha256ofString(rawNonce);

    try {
      // Request credential for the currently signed in Apple account.
      final appleCredential = await SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
        nonce: nonce,
      );

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Name from appleCredential: ${appleCredential.givenName} ${appleCredential.familyName}'),
      ));

      // Create an `OAuthCredential` from the credential returned by Apple.
      final oauthCredential = OAuthProvider('apple.com').credential(
        idToken: appleCredential.identityToken,
        rawNonce: rawNonce,
      );

      // Sign in the user with Firebase. If the nonce we generated earlier does
      // not match the nonce in `appleCredential.identityToken`, sign in will fail.
      await FirebaseAuth.instance.signInWithCredential(oauthCredential);
    } on SignInWithAppleAuthorizationException catch (exception) {
      if (exception.code != AuthorizationErrorCode.canceled) {
        rethrow;
      }
    }
  }

  Widget _buildLoginScreen(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text("You're currently disconnected."),
        ElevatedButton(
          child: const Text('CONNECT WITH APPLE'),
          onPressed: _handleAppleSignIn,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Testing Auth Apple'),
      ),
      body: Center(
        child: FractionallySizedBox(
          widthFactor: 0.8,
          child: !_authenticated
              ? _buildLoginScreen(context)
              : buildDisconnect(),
        ),
      ),
    );
  }
}
